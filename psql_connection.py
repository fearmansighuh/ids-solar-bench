import numpy as np
from random import uniform
import time
import psycopg2
from datetime import datetime


def testConnection(cursor):
    # use the cursor to interact with your database
    cursor.execute("SELECT 'hello world'")
    print(cursor.fetchone())


def main():
    #CONNECTION = "postgres://tsdbadmin:IDSUBCO2021I@d453176vyw.roaczgx3x7.tsdb.cloud.timescale.com:30750/tsdb?sslmode=require"
    #conn = psycopg2.connect(CONNECTION)
    conn = psycopg2.connect(dbname='iot_demo',
                            user='postgres',
                            password='password',
                            host='35.203.121.15',
                            port='5432')

    testConnection(conn.cursor())


if __name__ == "__main__":
    main()
